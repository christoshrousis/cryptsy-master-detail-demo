//
//  CryptsyManagerDelegate.h
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CryptsyManagerDelegate <NSObject>
- (void)didReceiveMarket:(NSArray *)market;
- (void)fetchingMarketFailedWithError:(NSError *)error;
@end
