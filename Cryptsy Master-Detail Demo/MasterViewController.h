//
//  MasterViewController.h
//  Cryptsy Master-Detail Demo
//
//  Created by Christos Hrousis on 19/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@end
