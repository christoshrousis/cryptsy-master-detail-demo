//
//  CryptsyManager.h
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CryptsyManagerDelegate.h"
#import "CryptsyCommunicatorDelegate.h"

@class CryptsyCommunicator;

@interface CryptsyManager : NSObject<CryptsyCommunicatorDelegate>
@property (strong, nonatomic) CryptsyCommunicator *communicator;
@property (weak, nonatomic) id<CryptsyManagerDelegate> delegate;

- (void)fetchMarket;

@end
