//
//  CryptsyCommunicator.m
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "CryptsyCommunicator.h"
#import "CryptsyCommunicatorDelegate.h"

@implementation CryptsyCommunicator
- (void)getMarketData
{
    NSString *urlAsString = [NSString stringWithFormat:@"http://pubapi.cryptsy.com/api.php?method=marketdatav2"];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
            [self.delegate fetchingMarketFailedWithError:error];
        } else {
            [self.delegate receivedMarketJSON:data];
        }
    }];
}
@end
