//
//  MasterViewController.m
//  Cryptsy Master-Detail Demo
//
//  Created by Christos Hrousis on 19/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"

#import "DetailCell.h"

#import "Market.h"
#import "CryptsyManager.h"
#import "CryptsyCommunicator.h"

@interface MasterViewController () <CryptsyManagerDelegate> {
    NSMutableArray *_objects;
    NSArray *_markets;
    CryptsyManager *_manager;
}
@end

@implementation MasterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Setup manager class and delegates.
    _manager = [[CryptsyManager alloc] init];
    _manager.communicator = [[CryptsyCommunicator alloc] init];
    _manager.communicator.delegate = _manager;
    _manager.delegate = self;
    
    //Add an spinner indicator to the masterview as a subview.
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(160, 240);
    spinner.tag = 12;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    //Start fetching the data.
    [_manager fetchMarket];
}

- (void)viewDidAppear
{
    [super viewDidAppear:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Cryptsy Manager Delegate

- (void)didReceiveMarket:(NSArray *)market
{
    _markets = market;
    [[self.view viewWithTag:12] removeFromSuperview]; // <-- Remove Spinner from master view because data has loaded.
    [self.tableView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:YES]; // <-- Forces the update to occur on the main thread.
    
}

- (void)fetchingMarketFailedWithError:(NSError *)error
{
    NSLog(@"Error %@; %@", error, [error localizedDescription]);
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _markets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Market *market = _markets[indexPath.row];
    [cell.marketLabel setText:market.marketName];
    [cell.detailedMarketsLabel setText:[NSString stringWithFormat:@"%@ -> %@", market.primaryName, market.secondaryName]];
    [cell.lastTradePriceLabel setText:market.lastTradePrice];

    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSMutableArray *object = _markets[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

@end
