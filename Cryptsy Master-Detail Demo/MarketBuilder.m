//
//  MarketBuilder.m
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "MarketBuilder.h"
#import "Market.h"

@implementation MarketBuilder

+ (NSArray *)marketFromJSON:(NSData *)objectNotation error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError != nil) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *theReturn = [jsonDict valueForKey:@"return"];
    NSDictionary *allMarkets = [theReturn valueForKey:@"markets"];
    
    NSDictionary *tempDictionary = [[NSDictionary alloc] init];
    for(id key in allMarkets)
    {
        tempDictionary = [allMarkets valueForKey:key];

        Market *market = [[Market alloc] init];
        market.marketName = key;
        market.primaryName = [tempDictionary valueForKey:@"primaryname"];
        market.primaryCode = [tempDictionary valueForKey:@"primarycode"];
        market.secondaryName = [tempDictionary valueForKey:@"secondaryname"];
        market.secondaryCode = [tempDictionary valueForKey:@"primarycode"];
        market.lastTradePrice = [tempDictionary valueForKey:@"lasttradeprice"];
        market.lastTradeTime = [tempDictionary valueForKey:@"lasttradetime"];
        
        [result addObject:market];
    }

    return result;
}

@end
