//
//  DetailViewController.m
//  Cryptsy Master-Detail Demo
//
//  Created by Christos Hrousis on 19/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "DetailViewController.h"
#import "Market.h"

@interface DetailViewController ()
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    //We are dangerously assuming we are recieving a market object...
    if (self.detailItem) {
        self.lastTradePriceLabel.text = [self.detailItem lastTradePrice];
        self.lastTradeTimeLabel.text = [NSString stringWithFormat:@"As of last trade time: %@", [self.detailItem lastTradeTime]] ;
        self.primaryNameLabel.text = [self.detailItem primaryName];
        self.secondaryNameLabel.text = [self.detailItem secondaryName];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
