//
//  DetailViewController.h
//  Cryptsy Master-Detail Demo
//
//  Created by Christos Hrousis on 19/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *primaryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondaryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastTradePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastTradeTimeLabel;

@end
